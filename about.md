---
layout: about
title: About Arts Accelerator
subtitle: An arts non profit
permalink: /about/
---

<h2 class="section-heading">Mission</h2>

---

The Arts Accelerator Fort Lauderdale is a non-profit dedicated to accelerating the careers of emerging artists by providing master classes facilitated by industry professionals, community events, performances, and screenings.
