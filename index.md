---
layout: rocket
title:  Rocket Film Workshop
subtitle: Launch your film career
date: "Lauch Date: August 18, 2018"
categories: Workshops 
cta-link: "http://bit.ly/aamasterclass-b"
---


<h2 class="section-heading">Call for Applications</h2>

---

ROCKET FILM is an intensive workshop for ambitious film artists ready to launch their careers with professional level work. 

ROCKET FILM WORKSHOP aims to improve the filmmaking skills and creative networks of its participants, and create content that puts South Florida’s film industry on national stage. 

This program is specialized for the needs of film artists, and includes a calling card short film masterclass, one-on-one coaching, and mentorship from individuals with experience at the top of their field. 

Selected participant projects will be promoted to regional and national film festivals and distributors.
 
ROCKET FILM WORKSHOP “Blueprint” will help you
 
* Create short format films that gets you noticed with resources you already have
* Develop compelling stories and characters
* Make films with the “secret ingredients” festival judges are looking for
* Avoid the tops mistakes that scream “Student Film”
* Develop robust production strategies for sustainable filmmaking
* Walk on to any set and be able to direct in less than 10 minutes.
* Create beautiful cinematography, even on a cell phone.
* Learn casting secrets that get you 90% of the way to success, before the shoot. 
 

<h2 class="section-heading">Program & Format</h2>

---

ROCKET FILM workshop will include:
* 3 sessions, 1 day each aimed at producing a finished short film 
* Step by Step development and production mentorship 
* Film screening and subsequent feedback
 

<h2 class="section-heading">Who Should Apply</h2>

---

Film Artists or teams looking to turn (or grow) their artistic practice into a sustainable career.


<h2 class="section-heading">Requirements</h2>

---

* The time to commit to this intensive program. Attendance is mandatory at all sessions, and research and planning is required between sessions.
* A strong desire and intention to turn their filmmaking passion into a full time career
* Willingness to receive honest feedback, advice and support
* Creative and collaborative flexibility

