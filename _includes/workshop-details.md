
<h2 class="section-heading">Eligibility</h2>

---

Rocket film participants will focus on one of the following tracks:

* Producers
* Writers
* Directors
* Cinematographers
* Editors
* Actors
* Sound Designers
* Motion || Effects Artists
* Makeup and Wardrobe Artists
* Creative Teams


<h2 class="section-heading">Price</h2>

---

* \$250 early bird
* \$300 regular
* \$350 at the door


<h2 class="section-heading">When</h2>

___

Aug. 24, 25 & 26  
Friday 7:00 PM - 11:00 PM, Saturday and Sunday 9:00 AM - 7:00 PM.  

\*Productions days may run longer than 7:00 PM based on shooting needs. 

<h2 class="section-heading">Where</h2>

---

Arts Accelerator Fort Lauderdale  
1310 SW 2nd Court Studio 101 Fort Lauderdale, FL 33312
 

<h2 class="section-heading">Reserve Your Seat!</h2>

---

Click the link below and register


