---
layout: rocket
title:  Rocket Film Workshop
subtitle: Make award winning films with resources you already have 
date: "Lauch Date: August 18, 2018"
categories: Workshops 
permalink: /workshop/
variant: workshop/
cta-link: "http://bit.ly/aamasterclass-b"
---


<h2 class="section-heading">Call for Applications</h2>

---

ROCKET FILM is an intensive workshop for help your make short films that accelerate your career with resources you already have.

Quick, Cheap, Good... Pick 3. 

If you're a filmmaker looking to win festival awards, hone your craft, or take your career to the next level, Rocket Film is for you.

<h2 class="section-heading">What You Will Learn</h2>

---
* “The Blueprint” for creating impactful short format films that gets you noticed
* The “secret ingredient” festival judges are looking for in your film
* Free/low cost alternatives to industry leading software and equipment 
* The 8 shots you need to make editing a breeze
* A practical framework for developing your story and characters
* Secrets to beautiful cinematography on a cell phone.
* Directing secret that gets you 90% of the way to success, before shooting your first shot. 
* The number one mistake to avoid that screams “Student Film”

<h2 class="section-heading">What You Will Get</h2>

---

A finished short film designed to accelerate your career, produced with an experienced mentor guiding you every step of the way.

